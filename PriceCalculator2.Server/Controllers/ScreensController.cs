﻿using PriceCalculator2.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PriceCalculator2.Server.Controllers
{
	[Route("api/[controller]")]
	public class ScreensController : Controller
	{
		[HttpGet("[action]")]
		public IEnumerable<Screen> GetScreens()
		{
			return new[]
				{new Screen() {Name = "Opera"}, new Screen() {Name = "Vilkpede"}, new Screen() {Name = "Birštonas"}};
		}
	}
}
